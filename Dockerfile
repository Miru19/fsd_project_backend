FROM node:14.18-buster

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN npm install
COPY . .

